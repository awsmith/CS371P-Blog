---
layout: post
title: "Week of 27 January–2 February 2020 (Second Entry)"
date: 2020-02-01 14:43:00 -0600
---

# What did you do this past week?
This week, I began by completing my annotations for the second paper, *Continuous Integration*, which I found to be a good general overview of the history and basic principles of the continuous integration development methodology.
Some of the advice given was a bit outdated, with the article having been written in 2006: a reference to Subversion as the primary version control tool used by professional engineers in particular attracted a few chuckles from me and my colleagues in the course, and a recommendation to push dependencies to version control has been made obsolete with the development of package managers for nearly every language.
Regardless, the core information about CI and enterprise software engineering as a whole was still valid and an interesting read, and would likely serve someone new to the concept well.

I also began my setup for the first project, *Collatz*, by forking the base repository on GitLab and setting up my issue tracker for the project.
I tried to base my issue labels on those I have seen in other successful open-source projects, with designations for different stages of a pseudo-Agile development process; I found GitLab's [scoped labels](https://gitlab.com/help/user/project/labels.md#scoped-labels-premium) particularly useful in creating a debugging workflow from *reported* &rarr; *confirmed* &rarr; *fixing* &rarr; *fixed*.
In addition to completing the repository setup, I finished setting up Docker on my primary development machine and successfully bootstrapped the CS 371P image.

# What's in your way?
The primary issue I face at the moment is my lack of familiarity with C++.
While I have some familiarity with older versions of the language, I am wholly uninformed on how to perform most tasks in idiomatic *modern* C++, often falling back to practices that would be more fitting for a C codebase.
In addition, I'm still fairly confused about references and how they behave: I can understand the basics of reference semantics&mdash;an `int&` is rougly analogous to an `int *` in C without requiring pointer syntax&mdash;but when concepts like [rvalue references](https://www.internalpointers.com/post/c-rvalue-references-and-move-semantics-beginners) begin to appear, I begin to wish for the simplicity of reference usage in Java and other higher-level languages.

# What will you do next week?
Next week, I intend to dive deeper into the project itself, implementing the na&iuml;ve solution and some preliminary optimisations.
I also plan to write [git hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) to automatically format the project according to `astyle` standards and ensure linter compliance on push, and to reformat the project according to my own formatting preferences, which differ from `astyle`'s, on pull so I can always interact with the code in my preferred style.
A few coworkers at my internship last summer adopted this practice for their work on our team's primary codebase, and I have since integrated it into my standard development workflow after seeing how it prevented any stylistic arguments from arising during code review.

# What was your prior experience with assertions, unit tests, coverage, and continuous integration?
In the past, I have used assertions and unit tests heavily in my own projects, which have generally demanded a high degree of code correctness due to their problem domains.
I was aware of the concept of test coverage and have always worked to ensure that all execution paths were tested, but I have never used coverage analysis tools to verify these assumptions.
I believe that they will help to alleviate the mental burden of having to trace code coverage myself and allow me to focus on writing comprehensive, high-quality tests.

I have used continuous integration for some small projects in the past, but have usually used a pre-built configuration instead of writing my own CI scripts.
While a script was also provided in the default repository for this project, I hope that future projects will allow me to write my own build scripts so that I can gain some more experience in that domain.
Also, fun fact: this blog is built using continuous integration, courtesy of GitLab Pages.

# What made you happy this week?
On Friday, I led three hour-long introductory C lectures for CS 429 (Computer Organization and Architecture) discussion sections.
These lectures went very well, and I could tell that the attendees, many of whom were my former students in CS 314 (Data Structures), gained a lot from them.
Thanks to Professor Chatterjee for granting me a platform to continue the tradition of the C lectures, which I have given every semester since Spring 2019.

# What's your pick of the week?
[This article](https://mgba.io/2020/01/25/infinite-loop-holy-grail/) on a Game Boy Advance emulator bug was extremely interesting and delved into the depths of the GBA's memory system and its impacts on CPU event timing.
As an emulator developer myself&mdash;albeit for the NES instead of the GBA&mdash;I greatly enjoyed this view into the complexities of emulating more modern systems, a field that I hope to explore in the near future.
For now though, I think I'll stick with debugging [undocumented 6502 opcodes](http://nesdev.com/undocumented_opcodes.txt) and getting CRT emulation to work without too many timing issues.