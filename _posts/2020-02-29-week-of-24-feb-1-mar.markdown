---
layout: post
title: "Week of 24 February–1 March 2020 (Fourth Entry)"
date: 2020-02-29 15:51:00 -0600
---

# What did you do this past week?
This week, I wrote my implementation of the *Voting* project and associated tasks.
I decided to take a bit of a departure from the recommended solution and write an object-oriented version of the election simulator with classes for ballots and candidates; while this improved the code quality substantially over a more unstructured approach and served as a good learning experience for writing classes in C++, it lent itself to some particularly pernicious bugs related to `std::vector` move semantics.
Furthermore, a `bool` instance variable that I forgot to initialise to `true` caused me a substantial headache until I learned to read Valgrind error messages, which eventually revealed where the issue was after compiling my project with debug symbols enabled.
I know from past experience that `clang-tidy` can catch these types of issues, so perhaps I'll add that to my development toolkit for the next project.

I also wrote a [Vim syntax highlighting plugin](https://github.com/Andrew-William-Smith/vim-checktestdata) for `checktestdata` format specifications, which I hope will prove itself useful as we continue writing `.ctd` files for the upcoming projects.
Regardless, I have been wondering how to write Vim syntax highlighting specifications for a while now, so developing this small extension served as good practice both with that task and with regular expressions.

# What's in your way?
Other than general fatigue from having three projects due this week, not much is standing in my way in this class.
I still have a periodic feeling of knowing not *quite* enough C++ to write optimal solutions to problems that I am presented with, but this feeling is subsiding with time and I expect that it will disappear entirely in the coming weeks.
*Voting* helped to increase my familiarity with common STL behaviours, and the in-class HackerRanks are proving very helpful in clarifying language details with which I was previously not entirely familiar.

# What will you do next week?
Next week, I plan to finish most of the next project&mdash;after procrastinating severely on *Voting*, I look forward to getting a jump start on my work next week now that I have fewer major assignments contending for my attention at once.
I also intend to make some improvements to my C++ development setup: I recently migrated from vanilla Vim to [Neovim](https://neovim.io/) and have had an excellent time thus far using [coc.nvim](https://github.com/neoclide/coc.nvim) for autocompletion and linting.
I plan to use [ccls](https://github.com/MaskRay/ccls) hooked into `coc` for autocomplete, linting, and refactoring support, which should bring my Vim C++ programming experience up to near feature parity with specialised IDE's like CLion and Visual Studio.

# What was your prior experience with containers, container adaptors, and iterators?
I have a good deal of experience with the concept of containers and iterators from past classes, and I have found the C++ container implementations to be similar to Java's in many ways.
However, that similarity has caused a few issues: in particular, the fact that `std::vector` does not store references&mdash;at least without an `std::reference_wrapper` template parameter&mdash;caused a difficult-to-track bug on the *Voting* project, and some of my other assumptions borne out of Java's standard library have not carried over to the STL.
Container adaptors are a new concept, but I can certainly see the utility that they provide.
While writing code in other languages, I have often been frustrated by the fact that building specialised containers necessitates implementing one or several interfaces on top of another data structure, so being able to specify the storage mechanism of a built-in container with no additional code is a welcome feature.

# What made you happy this week?
Aside from finishing the project on time, my organ practice is coming along well, with my rendition of [BWV 616 (*Mit Fried' und Freud' ich fahr' dahin*)](https://www.youtube.com/watch?v=BIfrUD3DOwo) nearing performance quality and my other pieces progressing at a decent pace.
The School of Music also granted my and my instructor's request for additional "stilts" for the bench at our largest practice organ this week (organs are not designed for taller people), so I have had a good deal of fun experimenting with the expanded registration options that that organ provides.
My favourite, and most excessive, thus far:

- **Grand:** 8' Spitz Principal, 4' Principal, 2' Principal, Mixture III, 8' Rohrflöte, 4' Gemshorn, 4' Viole de Gambe (Swell), 4' Trompette
- **Pedal:** 16' Spitz Principal, 8' Principal (Positive), 4' Principal (Positive), 16' Gedeckt, 8' Gedeckt, 8' Trompette

# What's your pick of the week?
This [list of 2020 leap day bugs](https://codeofmatt.com/list-of-2020-leap-day-bugs/) is equal parts amusing and sad: it serves as a good reminder that calendar code is incredibly hard to get right, so proper testing is absolutely crucial for any piece of software that has to deal with dates.
Alternatively, use a properly tested and vetted library to do any date computations for you, so when we inevitably switch to a [13-month calendar](https://en.wikipedia.org/wiki/International_Fixed_Calendar) all of your hard work won't go to waste.
