---
layout: post
title: "Week of 16–29 March 2020 (Fifth Entry)"
date: 2020-03-29 14:13:00 -0500
---

# Are you and your family safe and sound where you are?
My family lives in Dallas, Texas and are healthy and maintaining proper precautions to prevent infection.
I am staying in Austin through the end of the semester and housing my girlfriend who is a student at Princeton and is unable to return home after being evicted from the University as a result of the coronavirus outbreak.
We are fairly well-situated at the moment, with a few weeks' worth of provisions stockpiled in order to reduce the frequency with which we will need to expose ourselves to the public to buy groceries.
Overall, everyone is mostly happy and healthy, although a bit on-edge due to the restrictions imposed upon daily life.

# How do you feel about your ability to finish the term completely online?
To be honest, I am not confident at all about my performance through the rest of this semester with the transition to online classes.
I think that this class should be fine, as not much will change about the format; however, for some other classes like Algorithms, I feel that the situation may quickly become unworkable.
I also have a fair deal of difficulty learning when I am not physically present in a lecture hall, so Zoom classes will be a challenge, and I have already felt a decrease in my productivity due to being stuck at home.
For the most part, I am hoping that the College of Natural Sciences will follow the Faculty Council's recommendation to make classes taken on a pass/fail basis count for degree credit; otherwise, I will likely have to write off this entire semester and possibly drop my second major.

# What made you happy this week?
In addition to finishing the Allocator project and having my girlfriend in town, we have had the opportunity to do a lot of baking this week, which is always a relaxing activity for me.
My favourite item that we have made so far was an apple torte, although an experimental potato and leek quiche is a close second.
I have also taken the time to begin cultivating a new sourdough starter after the chloramine in Austin's public water supply killed off my last one&mdash;since we stocked up on filtered water before the stay-at-home order went into effect, I have plenty of starter-friendly water with which to restart the process.

# What's your pick of the week?
As we all adjust to the realities of life under quarantine, I feel like [this video by Bishop Robert Barron](https://www.youtube.com/watch?v=8_VJhBBqE5Y) on using the quarantine as an opportunity for quiet reflexion would benefit a lot of people, religious or not.
I have personally been working to deepen my spiritual life over the extended break even though religious services have been suspended for the time being, and I feel that doing so has done a good deal to maintain my mental health throughout this experience.
If you're not religious but still want to take some quiet time to reflect, I can also highly recommend mindfulness meditation as a way to center yourself and gain some time away from the calamity that is occupying so much of our attention nowadays.
